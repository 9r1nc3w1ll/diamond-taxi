package com.diamondpickup.taxi.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.craftman.cardform.Card;
import com.craftman.cardform.CardForm;
import com.craftman.cardform.OnPayBtnClickListner;
import com.diamondpickup.taxi.R;
import com.diamondpickup.taxi.model.M;

import android.os.AsyncTask;
import android.widget.Toast;

import co.paystack.android.Paystack;
import co.paystack.android.PaystackSdk;
import co.paystack.android.Transaction;
import co.paystack.android.model.Charge;

public class PayWithPayStack extends AppCompatActivity {
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_paystack);
    Button payBtn = findViewById(R.id.btn_pay);
    CardForm cardForm = findViewById(R.id.card_form);

    Intent intent = getIntent();
    TextView txtDes = (TextView)findViewById(R.id.payment_amount);
    txtDes.setText("₦" + intent.getStringExtra("amount"));
    payBtn.setText(String.format("Pay %s", txtDes.getText()));

    cardForm.setPayBtnClickListner(new OnPayBtnClickListner() {
      @Override
      public void onClick(Card card) {
        co.paystack.android.model.Card paystackCard = new co.paystack.android.model.Card(card.getNumber(), card.getExpMonth(), card.getExpYear(), card.getCVC());
        if (paystackCard.isValid()) {
          Charge charge = new Charge();
          charge.setCard(paystackCard);
          charge.setEmail("admin@gmail.com");
          charge.setAmount(Integer.parseInt(intent.getStringExtra("amount")) * 100);

          M.showLoadingDialog(PayWithPayStack.this);
          PaystackSdk.chargeCard(PayWithPayStack.this, charge, new Paystack.TransactionCallback() {
            @Override
            public void onSuccess(Transaction transaction) {
              String paymentReference = transaction.getReference();
              Toast.makeText(PayWithPayStack.this, "Transaction Successful!", Toast.LENGTH_SHORT).show();
              Intent returnIntent = new Intent();
              returnIntent.putExtra("result", transaction.getReference());
              setResult(Activity.RESULT_OK, returnIntent);
              M.hideLoadingDialog();
              finish();
            }

            @Override
            public void beforeValidate(Transaction transaction) {
            }

            @Override
            public void onError(Throwable error, Transaction transaction) {
              Toast.makeText(PayWithPayStack.this, "Transaction Failed!", Toast.LENGTH_SHORT).show();
              M.hideLoadingDialog();
              Log.e("Paystack", "TRANSACTION FAILED" + error.toString());
              Intent returnIntent = new Intent();
              returnIntent.putExtra("result", transaction.getReference());
              setResult(Activity.RESULT_CANCELED, returnIntent);
            }
          });

        } else {
          Toast.makeText(PayWithPayStack.this, "Invalid card details", Toast.LENGTH_SHORT).show();
        }
      }
    });
  }
}